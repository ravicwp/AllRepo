resource "kubernetes_daemonset" "test" {
  metadata {
    name      = "terraform-test"
    namespace = "something"
    labels = {
      test = "testravi"
    }
  }
  
  spec {
    selector {
      match_labels = {
        test = "testravi"
      }
    }
  }
}
